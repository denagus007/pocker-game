import React from 'react'
import Card from '../components/Card'

// this component recieves the available cards array and the addCard function as props
// then renders the display of communal cards
class CommunalContainer extends React.Component {
  render() {
    return (
      <div className="communal-container">
        {this.props.cards.map(card => (
          <Card
            key={card.name}
            code={card.code}
            // This handleClick function adds the current card to
            // the selected area in state
            handleClick={() => this.props.addCard(card)}
          />
        ))}
      </div>
    )
  }
}

export default CommunalContainer
