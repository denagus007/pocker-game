import React, { Component } from "react";
import Header from "./components/Header";
import CommunalContainer from "./containers/CommunalContainer";
import TableContainer from "./containers/TableContainer";
import OddsDisplay from "./components/OddsDisplay";
import cards from "./helpers/cardData";

class App extends Component {
  state = {
    cards: cards,
    player1: [],
    player2: [],
    player3: [],
    table: [],
    selected: "player1",
    limit: 5,
    oddsP1: null,
    oddsP2: null,
    oddsP3: null,
    oddsTie: null,
    ranks: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"],
    suits: ["c", "d", "h", "s"]
  };

  availableCards = () => {
    return this.state.cards.filter((card) => {
      return (
        !this.state.player1.includes(card) &&
        !this.state.player2.includes(card) &&
        !this.state.player3.includes(card) &&
        !this.state.table.includes(card)
      );
    });
  };

  addCard = (selectedCard) => {
    if (this.state[this.state.selected].length < this.state.limit) {
      this.setState({
        [this.state.selected]: [
          ...this.state[this.state.selected],
          selectedCard
        ]
      });

      this.state[this.state.selected].push(selectedCard);
    }
  };

  setSelected = (area) => this.setState({ selected: area });

  getOdds = () => {
    let res = [];
    let _ranks = this.state.ranks;
    for (let i = 1; i <= 3; i++) {
      let cards = _sanitise(this.state["player" + i]);
      let best = _result(cards);

      for (let combination of _combinations(cards, 5)) {
        // calculate value of 5 cards
        let result = _calculate(combination);
        if (result.value > best.value) best = result;
      }

      // console.log(best);

      function _result(cards, name, value, whoami) {
        return {
          cards: cards,
          name: name || "nothing",
          value: value || 0,
          whoami: whoami || ""
        };
      }

      function _sanitise(allCards) {
        // concatenate
        let cards = [].concat.apply([], allCards);
        // valid rank and suit
        cards = cards.filter((card) => {
          return !!(_ranks.indexOf(card.rank) > -1 && card.suit);
        });

        return cards;
      }

      function _combinations(cards, groups) {
        // card combinations with the given size
        let result = [];

        // not enough cards
        if (groups > cards.length) return result;

        // one group
        if (groups == cards.length) return [cards];

        // one card in each group
        if (groups == 1) return cards.map((card) => [card]);

        // everything else
        for (let i = 0; i < cards.length - groups; i++) {
          let head = cards.slice(i, i + 1);
          let tails = _combinations(cards.slice(i + 1), groups - 1);
          for (let tail of tails) {
            result.push(head.concat(tail));
          }
        }

        return result;
      }

      function _ranked(cards) {
        // split cards by rank
        let result = [];

        for (let card of cards) {
          let r = _ranks.indexOf(card.rank);
          result[r] = result[r] || [];
          result[r].push(card);
        }

        // condense
        result = result.filter((rank) => !!rank);

        // high to low
        result.reverse();

        // pairs and sets first
        result.sort((a, b) => {
          return a.length > b.length ? -1 : a.length < b.length ? 1 : 0;
        });

        return result;
      }

      function _isFlush(cards) {
        // all suits match is flush
        let suit = cards[0].suit;

        for (let card of cards) {
          if (card.suit != suit) return false;
        }

        return true;
      }

      function _isStraight(ranked) {
        // must have 5 different ranks
        if (!ranked[4]) return false;

        // could be wheel if r0 is 'ace' and r4 is '2'
        if (
          ranked[0][0].rank == "ace" &&
          ranked[1][0].rank == "5" &&
          ranked[4][0].rank == "2"
        ) {
          // hand is 'ace' '5' '4' '3' '2'
          ranked.push(ranked.shift());
          // ace is now low
          return true;
        }

        // run of five in row is straight
        let r0 = _ranks.indexOf(ranked[0][0].rank);
        let r4 = _ranks.indexOf(ranked[4][0].rank);
        return r0 - r4 == 4;
      }

      function _value(ranked, primary) {
        // primary wins the rest are kickers
        let str = "";

        for (let rank of ranked) {
          // create two digit value
          let r = _ranks.indexOf(rank[0].rank);
          let v = (r < 10 ? "0" : "") + r;
          for (let i = 0; i < rank.length; i++) {
            // append value for each card
            str += v;
          }
        }

        // to integer
        return primary * 10000000000 + parseInt(str);
      }

      function _calculate(cards) {
        // determine value of hand

        let ranked = _ranked(cards);
        let isFlush = _isFlush(cards);
        let isStraight = _isStraight(ranked);

        if (isStraight && isFlush && ranked[0][0].rank == "ace")
          return _result(
            cards,
            "royal flush",
            _value(ranked, 9),
            "Player " + i
          );
        else if (isStraight && isFlush)
          return _result(
            cards,
            "straight flush",
            _value(ranked, 8),
            "Player " + i
          );
        else if (ranked[0].length == 4)
          return _result(
            cards,
            "four of a kind",
            _value(ranked, 7),
            "Player " + i
          );
        else if (ranked[0].length == 3 && ranked[1].length == 2)
          return _result(cards, "full house", _value(ranked, 6), "Player " + i);
        else if (isFlush) return _result(cards, "flush", _value(ranked, 5));
        else if (isStraight)
          return _result(cards, "straight", _value(ranked, 4), "Player " + i);
        else if (ranked[0].length == 3)
          return _result(
            cards,
            "three of a kind",
            _value(ranked, 3),
            "Player " + i
          );
        else if (ranked[0].length == 2 && ranked[1].length == 2)
          return _result(cards, "two pair", _value(ranked, 2), "Player " + i);
        else if (ranked[0].length == 2)
          return _result(cards, "one pair", _value(ranked, 1), "Player " + i);
        else
          return _result(cards, "high card", _value(ranked, 0), "Player " + i);
      }

      res.push(best);
    }

    this.setState({
      oddsP1: res[0].name + ", Score = " + res[0].value,
      oddsP2: res[1].name + ", Score = " + res[1].value,
      oddsP3: res[2].name + ", Score = " + res[2].value
    });

    res = res.sort((a, b) => b.value - a.value);

    this.setState({
      oddsTie: res[0].whoami
    });
  };

  playGame = () => {
    this.state.table = [];

    for (let i = 0; i < 3; i++) {
      this.state.selected = "player" + (i + 1);

      this.setState({ [this.state.selected]: [] });
      this.state[this.state.selected] = [];

      let p = [];
      for (let c = 1; c <= this.state.limit; c++) {
        let a = this.availableCards()[
          Math.floor(Math.random() * this.availableCards().length)
        ];
        this.state.table.push(a);
        p.push(a);
      }

      let sortedHand = [];
      for (let i = 0; i < this.state.ranks.length; i++) {
        for (let j = 0; j < p.length; j++) {
          if (p[j].rank == this.state.ranks[i]) {
            sortedHand.push(p[j]);
          }
        }
      }

      for (let a = 0; a < sortedHand.length; a++) {
        this.addCard(sortedHand[a]);
      }
    }
  };

  render() {
    const communalCards = this.availableCards();
    // The above constant will be passed as a prop
    // to one of our container components
    return (
      <div>
        <Header />
        <CommunalContainer cards={communalCards} addCard={this.addCard} />
        <button className="active-button" onClick={this.playGame}>
          Play Game
        </button>
        <div className="table-containers">
          <TableContainer
            name="Player 1"
            area="player1"
            cards={this.state.player1}
            // removeCard={this.removeCard}
            setSelected={this.setSelected}
            selected={this.state.selected}
          />
          <TableContainer
            name="Player 2"
            area="player2"
            cards={this.state.player2}
            // removeCard={this.removeCard}
            setSelected={this.setSelected}
            selected={this.state.selected}
          />
          <TableContainer
            name="Player 3"
            area="player3"
            cards={this.state.player3}
            // removeCard={this.removeCard}
            setSelected={this.setSelected}
            selected={this.state.selected}
          />
        </div>
        <OddsDisplay
          getOdds={this.getOdds}
          odds1={this.state.oddsP1}
          odds2={this.state.oddsP2}
          odds3={this.state.oddsP3}
          oddsTie={this.state.oddsTie}
        />
      </div>
    );
  }
}

export default App;
