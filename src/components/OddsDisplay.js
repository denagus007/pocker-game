import React from "react";

class OddsDisplay extends React.Component {
  render() {
    return (
      <div className="odds-container">
        {/* The button below calculates the odds for a query */}
        <button className="active-button" onClick={this.props.getOdds}>
          Who is the winner?
        </button>
        <div className="results-container">
          {/* The figures we get back from the odds calculation 
          will be displayed in the p tags below */}
          <p>Player 1: {this.props.odds1}</p>
          <p>Player 2: {this.props.odds2}</p>
          <p>Player 3: {this.props.odds3}</p>
          <p>Result: {this.props.oddsTie} Win</p>
        </div>
      </div>
    );
  }
}

export default OddsDisplay;
