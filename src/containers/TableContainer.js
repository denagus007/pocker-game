import React from "react";
import Card from "../components/Card";

class TableContainer extends React.Component {
  // The line below sets the button to be an active button, based on whether the current area
  // is the selected area
  render() {
    const buttonClass = "";
    return (
      <div className="table-card-containers">
        {/* The button below sets the selected area to be the current area in state */}
        <button className={buttonClass}>{`${this.props.name} Cards`}</button>
        <div>
          {this.props.cards.map((card) => (
            <Card
              key={card.name}
              code={card.code}
              // The handleclick function below removes the card from
              // the current area in state
              handleClick={() => this.props.removeCard(card, this.props.area)}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default TableContainer;
