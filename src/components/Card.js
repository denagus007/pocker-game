import React from "react";

/* This component should return an image tag that 
      displays an individual card which runs a callback
      function (handleClick) when it is clicked on.*/
class Card extends React.Component {
  render() {
    return (
      <img
        src={`/imgs/cards/${this.props.code}.png`}
        alt="card"
        // onClick={this.props.handleClick}
      />
    );
  }
}

export default Card;
