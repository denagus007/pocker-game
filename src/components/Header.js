import React from "react";

class Header extends React.Component {
  render() {
    return (
      <div>
        <h1>Poker Game</h1>
        <p>
          The game starts, and the deck shuffled then 5 cards dealt to each
          player, called a hand. Upon this point each player will show their
          hands, and they are ranked as below from highest to lowest. The game
          must determine a winner based on the highest hand and announce the
          winner.
        </p>
      </div>
    );
  }
}

export default Header;
